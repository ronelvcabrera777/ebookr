<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="jquery/jquery-3.3.1.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
<link href="bootstrap/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<link href="css/ebookr.css" rel="stylesheet" type="text/css" />
<title>eBookr</title>
</head>
<body>
	<div class="full-wide header_banner_ctn">
		<div class="trapezoid"></div>
	</div>
	<div class="full-wide body_ctn">
		<div class="login_form_ctn">
			<div class="login_form">
				<html:form action="/newContact" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label for="firstName">First name</label> 
						<input type="text" class="form-control" id="firstName" name="firstName" placeholder="Enter First Name">
					</div>
					<div class="form-group">
						<label for="lastName">Last name</label> 
						<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Enter Last Name">
					</div>
					<div class="form-group">
						<label for="middleName">Middle name</label> 
						<input type="text" class="form-control" id="middleName" name="middleName" placeholder="Enter Middle Name">
					</div>
					<div class="form-group">
						<label for="mobileNumber">Mobile Number</label> 
						<input type="text" class="form-control" id="mobileNumber" name="mobileNumber" placeholder="Enter Mobile Number">
					</div>
					<div class="form-group">
						<label for="country">Country</label> 
						<select class="form-control" id="country" name="country">
							<option value=0>Select Country</option>
						</select>
					</div>
					<div class="form-group">
						<label for="City">City</label> 
						<select class="form-control" id="city" name="city">
							<option value=0>Select City</option>
						</select>
					</div>
					<div class="form-group">
					    <label for="profileImage">Profile Image</label>
						<bean:message key="label.common.file.profileImage.label"></bean:message>
						<html:file property="profileImage" size="50"> </html:file>
				    </div>
					<div class="form-group">
					    <label for="signatureImage">Signature</label>
						<bean:message key="label.common.file.signatureImage.label"></bean:message>
						<html:file property="signatureImage" size="50"> </html:file>
				    </div>
				    <input type="hidden" name="createdById" id="createdById" value=1>
					<button type="submit" class="btn btn-primary">Submit</button>
				</html:form>
			</div>
		</div>
	</div>
</body>
</html>