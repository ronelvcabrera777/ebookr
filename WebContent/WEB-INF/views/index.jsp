<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<mvc:resources mapping="/**" location="/WEB-INF"/>
	<script src="/jquery/jquery-3.3.1.min.js"></script>
	<script src="/bootstrap/js/bootstrap.js"></script>
	<link href="/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="/css/ebookr.css" rel="stylesheet" type="text/css" />
<title>eBookr</title>
</head>
<body>
	<div class="full-wide header_banner_ctn">
		<div class="header_logo_ctn"></div>
	</div>
	<div class="full-wide body_ctn">
	</div>
</body>
</html>