<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script src="jquery/jquery-3.3.1.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>

<link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="css/ebookr.css" rel="stylesheet" type="text/css" />

<title>eBookr</title>
</head>
<body>
	<div class="full-wide body_ctn">
		<div class="login_form_ctn">
			<div class="row">
				<div class="col-md-12">
					<img id="logo-img" style="max-width:100%; max-height:100%;;" src="img/logo.png"/>
				</div>
				<div class="col-md-12">
					<div class="login_form">
						<html:form action="/login">
							<div class="form-group">
								<label for="username">Username</label> 
								<input type="email" class="form-control" id="username" name="username" aria-describedby="usernameHelp" placeholder="Username"> 
								<small id="usernameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
							</div>
							<div class="form-group">
								<label for="password">Password</label> 
								<input type="password" class="form-control" id="password" name="password" placeholder="Password">
							</div>
							<button type="submit" class="btn btn-primary">Login</button>
							<button type="submit" class="btn btn-primary">Register</button>
						</html:form>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>
</html>