/**
 * 
 */


function init() {
	
	if(firstContactId != null) {
		
		activateContact(firstContactId);
	}
	
	setListener();
}

function setListener() {

	$("#country").change(function() {
		
		var cities = cityList[$(this).val()];
		var cityEl = $("#city");
		cityEl.empty();

		cityEl.append($("<option></option>")
			.attr("value", 0).text("Select City"));
		
		$.each(cities, function(key, value) {
			cityEl.append($("<option></option>")
				.attr("value", value[0]).text(value[1]));
		});
	});
}

function activateContact(contactId) {
	
	var container = document.getElementById("contact-grid-entry-handler-" + contactId);
	var parentContainer = document.getElementById("contact-grid-ctn");
	
	if(!container)
		return;
	
	// Deactive selection
	for(var loop = 0; loop < parentContainer.childNodes.length; loop++) {
		
		var element = parentContainer.childNodes[loop];
		
		if(!element.className)
			continue;
		
		if(element.classList.contains("active")) {
			
			element.classList.remove("active");
			element.classList.add("inactive");
			break;
		}
	}
	
	var city = document.getElementById("cityId-" + contactId).value;
	var country = document.getElementById("countryId-" + contactId).value;
	var firstName = document.getElementById("firstName-" + contactId).value;
	var lastName = document.getElementById("lastName-" + contactId).value;
	var middleName = document.getElementById("middleName-" + contactId).value;
	var mobileNumber = document.getElementById("mobileNumber-" + contactId).value;
	var zipCode = document.getElementById("zipCode-" + contactId).value;
	var profileImage = document.getElementById("profileImage-" + contactId).value;
	var signatureImage = document.getElementById("signatureImage-" + contactId).value;
	var slug = document.getElementById("slug-" + contactId).value;

	document.getElementById("fullname-text").innerHTML = firstName + " " + middleName + " " + lastName;
	document.getElementById("mobile-number-text").innerHTML = mobileNumber;
	document.getElementById("country-text").innerHTML = country;
	document.getElementById("city-text").innerHTML = city;
	document.getElementById("zip-code-text").innerHTML = zipCode;
	document.getElementById("profileimage-img").src = "upload/" + profileImage;
	document.getElementById("active-record").value = contactId;
	
	container.classList.add('active');
	container.classList.remove('inactive');
}

function exportContact() {
	
	document.getElementById("exportContactListId").value = document.getElementById("active-record").value;
	$("#exportContactForm")[0].submit();
}

function updateContactById(contactId) {

	var contactId = document.getElementById("active-record").value;
	
	var city = document.getElementById("city-" + contactId).value;
	var country = document.getElementById("country-" + contactId).value;
	var firstName = document.getElementById("firstName-" + contactId).value;
	var lastName = document.getElementById("lastName-" + contactId).value;
	var middleName = document.getElementById("middleName-" + contactId).value;
	var mobileNumber = document.getElementById("mobileNumber-" + contactId).value;
	var zipCode = document.getElementById("zipCode-" + contactId).value;
	var profileImage = document.getElementById("profileImage-" + contactId).value;
	var signatureImage = document.getElementById("signatureImage-" + contactId).value;
	var slug = document.getElementById("slug-" + contactId).value;

	document.getElementById("firstName").value = firstName;
	document.getElementById("lastName").value = lastName;
	document.getElementById("middleName").value = middleName;
	document.getElementById("mobileNumber").value = mobileNumber;
	document.getElementById("country").value = country;
	$("#country").trigger('change'); 
	document.getElementById("city").value = city;
	document.getElementById("updateContactListId").value = contactId;
	
	$('#contactDetailModal').modal('show')
}

function updateContact() {
	
	updateContactById(document.getElementById("active-record").value);
}

function deleteContactButton() {
	
	deleteContact(document.getElementById("active-record").value)
}

function deleteContact(contactId) {
	
	var firstName = document.getElementById("firstName-" + contactId).value;
	var lastName = document.getElementById("lastName-" + contactId).value;
	var middleName = document.getElementById("middleName-" + contactId).value;
	
	document.getElementById("contactListId").value = contactId;
	document.getElementById("delete-fullname").innerHTML = firstName + " " + middleName + " " + lastName;
	$('#deleteModal').modal('show')
}