<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.List, org.cabrera.ebookr.model.ContactList, org.cabrera.ebookr.model.AdCountry, org.cabrera.ebookr.model.AdCity"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="jquery/jquery-3.3.1.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
<script src="js/contacts.js"></script>

<link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="css/ebookr.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>eBookr</title>
<script>
	var firstContactId = null;
	var cityList = [];
</script>
	<logic:iterate name="cityList" id="city">
		
		<script type="text/javascript">
			var index = '<bean:write name="city" property="adCountry.adCountryId"/>';
			
			if(cityList[index] == undefined)
				cityList[index] = [];
			
            cityList[index].push(['<bean:write name="city" property="adCityId"/>', '<bean:write name="city" property="name"/>']);
        </script>
	</logic:iterate>
</head>
<body onload="init()">
	<div class="full-wide header_banner_ctn">
		<img id="logo-img" style="max-width:100%; max-height:100%;;" src="img/logo.png"/>
	</div>
	<div class="full-wide body_ctn">
		<div class="contact-list-ctn">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div align="center">
						<span class="header-title">My Contacts</span>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<br/>
					<div class="contact-list">
						<div class="inner-content search-bar-ctn">
							<div class="row">	
								<div class="col-sm-9 col-md-9">
									<input type="text" class="form-control" id="searchinput" name="searchinput" placeholder="Search contact...">
								</div>
								<div class="col-sm-3 col-md-3">
									<button type="button" class="btn fa fa-plus-circle" data-toggle="modal" data-target="#contactDetailModal">
									</button>
								</div>
							</div>
						</div>
						<br/>
						<div id="contact-grid-ctn" class="inner-content contacts-grid-ctn">
						<%
							List<ContactList> data = (List<ContactList>) request.getAttribute("contactList");
						
							for(int loop = 0; loop < data.size(); loop++) {
								
								String className = "inactive";
								
								if(loop == 0) {
									className = "active";
									%>
									<script>
										firstContactId = <%= data.get(loop).getContactListId() %>;
									</script>
									<%
								}
					
						%>
								<div id="contact-grid-entry-handler-<%= data.get(loop).getContactListId() %>" class="contact-entry-ctn <%= className %>" onclick="activateContact(<%= data.get(loop).getContactListId() %>)">
									<div class="contact-preview-ctn">
											<span><%= data.get(loop).getFirstName() + " " + data.get(loop).getMiddleName() + " " + data.get(loop).getLastName() %></span>
										<div class="fullname-ctn"></div>
										<div class="mobile-number-ctn">
											<span><%= data.get(loop).getMobileNumber() %></span>
										</div>
									</div>	
									<div align="right" class="button-action-ctn">
										<i class="fa fa-edit" onclick="updateContactById(<%= data.get(loop).getContactListId() %>)"></i>
										<i class="fa fa-trash" onclick="deleteContact(<%= data.get(loop).getContactListId() %>)"></i>
									</div>
								</div>
								<input type="hidden" name="contactListId-<%= data.get(loop).getContactListId() %>" id="contactListId-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getContactListId() %>">
								<input type="hidden" name="cityId-<%= data.get(loop).getContactListId() %>" id="cityId-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getAdCity().getName() %>">
								<input type="hidden" name="city-<%= data.get(loop).getContactListId() %>" id="city-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getAdCity().getAdCityId() %>">
								<input type="hidden" name="countryId-<%= data.get(loop).getContactListId() %>" id="countryId-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getAdCountry().getName() %>">
								<input type="hidden" name="country-<%= data.get(loop).getContactListId() %>" id="country-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getAdCountry().getAdCountryId() %>">
								<input type="hidden" name="firstName-<%= data.get(loop).getContactListId() %>" id="firstName-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getFirstName() %>">
								<input type="hidden" name="lastName-<%= data.get(loop).getContactListId() %>" id="lastName-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getLastName() %>">
								<input type="hidden" name="middleName-<%= data.get(loop).getContactListId() %>" id="middleName-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getMiddleName() %>">
								<input type="hidden" name="mobileNumber-<%= data.get(loop).getContactListId() %>" id="mobileNumber-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getMobileNumber() %>">
								<input type="hidden" name="zipCode-<%= data.get(loop).getContactListId() %>" id="zipCode-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getZipCode() %>">
								<input type="hidden" name="profileImage-<%= data.get(loop).getContactListId() %>" id="profileImage-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getProfileImage() %>">
								<input type="hidden" name="signatureImage-<%= data.get(loop).getContactListId() %>" id="signatureImage-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getSignatureImage() %>">
								<input type="hidden" name="slug-<%= data.get(loop).getContactListId() %>" id="slug-<%= data.get(loop).getContactListId() %>" value="<%= data.get(loop).getSlug() %>">
						<%
							}
						%>
						</div>
					</div>
					<br/>
				</div>
				<div class="col-sm-8 col-md-8">
					<br/>
					<div class="contact-detail">
						<div class="row">
							<div class="col-sm-12 col-md-12">
								<div align="center">
									<input type="hidden" id="active-record" class="active-record" value="">
									<button type="button" class="btn btn-primary" onclick="updateContact()">Edit</button>
									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" onclick="deleteContactButton()">Delete</button>
									<button type="button" class="btn btn-info" onclick="exportContact()">Export</button>
								</div>
							</div>
							<div class="col-sm-12 col-md-12">
								<div class="row">
									<div class="col-md-4 offset-md-4">
										<div style="height: 200px;" align="center">
											<img id="profileimage-img" style="height: 100%;" src=""/>
										</div>
									</div>
									<div class="col-md-6 offset-md-3">
										<div align="center">
											<span id="fullname-text" class="contact-text-detail"></span>
										</div> <br/>
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-md-12">
								<div class="row">
									<div class="col-sm-12 col-md-10 offset-md-1">
										<div class="row">
											<div class="col-sm-4 col-md-4">
												<span class="contact-text-detail"> Mobile Number:</span>
											</div>
											<div class="col-sm-8 col-md-8">
												<span id="mobile-number-text" class="contact-text-detail"></span>
											</div>
										</div> <br/>
										<div class="row">
											<div class="col-sm-4 col-md-4">
												<span class="contact-text-detail"> Country:</span>
											</div>
											<div class="col-sm-8 col-md-8">
												<span id="country-text" class="contact-text-detail"></span>
											</div>
										</div> <br/>
										<div class="row">
											<div class="col-sm-4 col-md-4">
												<span class="contact-text-detail"> City:</span>
											</div>
											<div class="col-sm-8 col-md-8">
												<span id="city-text" class="contact-text-detail"></span>
											</div>
										</div> <br/>
										<div class="row">
											<div class="col-sm-4 col-md-4">
												<span class="contact-text-detail"> Zip Code:</span>
											</div>
											<div class="col-sm-8 col-md-8">
												<span id="zip-code-text" class="contact-text-detail"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<br/>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title" id="deleteModalLabel">Delete contact</h5>
				  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<span>Are you sure you want to delete </span><span id="delete-fullname"></span><span> ?</span>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<html:form action="/deleteContact" method="post" enctype="multipart/form-data">
						<input type="hidden" id="contactListId" name="contactListId" value="">
					  	<input type="submit" class="btn btn-danger" value="Yes">
					  	<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
					</html:form>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="contactDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title" id="exampleModalLabel">Contact Details</h5>
				  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
				<html:form action="/newContact" method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="form-group">
							<label for="firstName">First name</label> 
							<input type="text" class="form-control" id="firstName" name="firstName" placeholder="Enter First Name" required>
						</div>
						<div class="form-group">
							<label for="lastName">Last name</label> 
							<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Enter Last Name" required>
						</div>
						<div class="form-group">
							<label for="middleName">Middle name</label> 
							<input type="text" class="form-control" id="middleName" name="middleName" placeholder="Enter Middle Name" required>
						</div>
						<div class="form-group">
							<bean:message key="label.common.file.profileImage.label"></bean:message>
							<html:file property="profileImage" size="50"> </html:file>
				    	</div>
						<div class="form-group">
							<label for="mobileNumber">Mobile Number</label> 
							<input type="text" class="form-control" id="mobileNumber" name="mobileNumber" placeholder="Enter Mobile Number" required>
						</div>
						<div class="form-group">
							<label for="country">Country</label> 
							<select class="form-control" id="country" name="country" required> 
								<option value=0>Select Country</option>
								<%

									List<AdCountry> country = (List<AdCountry>) request.getAttribute("countryList");
								
									for(int loop = 0; loop < country.size(); loop++) {
								%>
									<option value=<%=country.get(loop).getAdCountryId() %>><%=country.get(loop).getName() %></option>
								<%
									}
								%>
							</select>
						</div>
						<div class="form-group">
							<label for="City">City</label> 
							<select class="form-control" id="city" name="city" required>
								<option value=0>Select City</option>
							</select>
						</div>
						<div class="form-group">
							<bean:message key="label.common.file.signatureImage.label"></bean:message>
							<html:file property="signatureImage" size="50"> </html:file>
			    		</div>
					</div>
				    <input type="hidden" name="createdById" id="createdById" value="<bean:write name="userAccount" property="userAccountId"/>">
				    <input type="hidden" name="updateContactListId" id="updateContactListId" value="">
					<div class="modal-footer">
					  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					  <input type="submit" class="btn btn-primary" value="Submit">
					</div>
				</html:form>
			</div>
		</div>
	</div>
	
	<html:form action="/exportContact" styleId="exportContactForm" method="post" enctype="multipart/form-data" target="_blank">
	    <input type="hidden" id="exportContactListId" name="contactListId" value=""/>">
	</html:form>
</body>
</html>