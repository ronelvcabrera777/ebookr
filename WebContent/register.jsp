<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="jquery/jquery-3.3.1.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
<link href="bootstrap/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<link href="css/ebookr.css" rel="stylesheet" type="text/css" />
<title>eBookr</title>
</head>
<body>
	<div class="full-wide header_banner_ctn">
		<div class="trapezoid"></div>
	</div>
	<div class="full-wide body_ctn">
		<div class="login_form_ctn">
			<div class="login_form">
				<html:form action="/register">
					<div class="form-group">
						<label for="firstName">First name</label> 
						<input type="text" class="form-control" id="firstName" name="firstName" placeholder="Enter First Name">
					</div>
					<div class="form-group">
						<label for="lastName">Last name</label> 
						<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Enter Last Name">
					</div>
					<div class="form-group">
						<label for="emailAddress">Email Address</label> 
						<input type="email" class="form-control" id="emailAddress" name="emailAddress" aria-describedby="usernameHelp" placeholder="Enter email address"> 
						<small id="usernameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
					</div>
					<div class="form-group">
						<label for="password">Password</label> 
						<input type="password" class="form-control" id="password" name="password" placeholder="Password">
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
				</html:form>
			</div>
		</div>
	</div>
	</div>
</body>
</html>