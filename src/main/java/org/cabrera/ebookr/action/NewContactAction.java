package org.cabrera.ebookr.action;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.cabrera.ebookr.form.ContactsForm;
import org.cabrera.ebookr.model.UserAccount;
import org.cabrera.ebookr.services.ContactListService;

public class NewContactAction extends BaseAction {
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		ContactsForm contactForm = (ContactsForm) form;
		ActionForward forward = mapping.getInputForward();
		
		if(contactForm.getUpdateContactListId() != 0) {
			
			if(this.updateContact(contactForm, request) == null) {

				forward = mapping.findForward("success");
			}
		} else {
			if(this.createNewContact(contactForm, request) == null) {
				forward = mapping.findForward("success");
			} else {
				
				ActionErrors err = new ActionErrors();
				err.add("", new ActionMessage("login.fail"));
				this.saveErrors(request, err);
			}
		}

		return forward;
	}
	
	public String updateContact(ContactsForm contact, HttpServletRequest request) throws Exception {
		
		ContactListService service = new ContactListService();
		service.setUserAccount(contact.getCreatedById());
		service.setAdCity(contact.getCity());
		service.setAdCountry(contact.getCountry());
		service.seTAdContactListId(contact.getUpdateContactListId());
		
		String newContact;
		String newProfileImageFilename = "";
		String newSignatureFilename = "";
		
		try {
			
			newProfileImageFilename = service.uploadFile(contact.getProfileImage(), this.getServlet());
			newSignatureFilename = service.uploadFile(contact.getSignatureImage(), this.getServlet());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		contact.setNewProfileImage(newProfileImageFilename);
		contact.setNewSignatureImage(newSignatureFilename);
		
		if((newContact = service.updateContact(contact)) != null) {
			return newContact;
		}
		
		UserAccount userAccount = (UserAccount) request.getSession().getAttribute("userAccount");
		this.setVariables(userAccount, service.getEntityManager(), request);
		
		return null;
	}
	
	public String createNewContact(ContactsForm contact, HttpServletRequest request) throws Exception {
		
		ContactListService service = new ContactListService();
		service.setUserAccount(contact.getCreatedById());
		service.setAdCity(contact.getCity());
		service.setAdCountry(contact.getCountry());
		
		String newContact;
		String newProfileImageFilename = "";
		String newSignatureFilename = "";
		
		try {
			
			newProfileImageFilename = service.uploadFile(contact.getProfileImage(), this.getServlet());
			newSignatureFilename = service.uploadFile(contact.getSignatureImage(), this.getServlet());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		contact.setNewProfileImage(newProfileImageFilename);
		contact.setNewSignatureImage(newSignatureFilename);
		
		if((newContact = service.addNewContact(contact)) != null) {
			return newContact;
		}
		
		UserAccount userAccount = (UserAccount) request.getSession().getAttribute("userAccount");
		this.setVariables(userAccount, service.getEntityManager(), request);
		
		return null;
	}
}
