package org.cabrera.ebookr.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.cabrera.ebookr.form.ContactsForm;
import org.cabrera.ebookr.model.ContactListDetailV;
import org.cabrera.ebookr.services.ContactListService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

public class ExportContactAction extends Action {
	
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		String outputFileName = "contactDetail_" + timeStamp + ".pdf";
		String outputFile = this.getServlet().getServletContext().getRealPath("/report");
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=" + outputFileName);
		
		ContactsForm contactForm = (ContactsForm) form;

		File jasperFile = new File(this.getServlet().getServletContext().getRealPath("/jasper/contactDetails.jasper"));
		ContactListService service = new ContactListService();
		ContactListDetailV contact = service.getContact(contactForm.getContactListId());
		List<ContactListDetailV> data = new ArrayList<ContactListDetailV>();
		data.add(contact);

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("contact_list_id", contactForm.getContactListId());
		parameters.put("URL", this.getServlet().getServletContext().getRealPath("/"));
		
		File reportFile = new File(this.getServlet().getServletContext().getRealPath("/report"));
		
		if(!reportFile.exists()) {
			reportFile.mkdirs();
		}
		
		if(jasperFile.exists()) {
			
			JRBeanCollectionDataSource dataBeanColl = new JRBeanCollectionDataSource(data);
			
			JasperReport jasper = (JasperReport) JRLoader.loadObject(jasperFile);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasper, parameters, dataBeanColl); 
			
			OutputStream outputStream = new FileOutputStream(new File(outputFile + "/" + outputFileName));
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
			
			outputStream.flush();
			outputStream.close();
			
			
		}

		return null;
	}
}
