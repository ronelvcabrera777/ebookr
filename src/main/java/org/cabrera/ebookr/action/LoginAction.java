package org.cabrera.ebookr.action;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.cabrera.ebookr.form.LoginForm;
import org.cabrera.ebookr.model.UserAccount;
import org.cabrera.ebookr.services.UserAccountService;

public class LoginAction extends BaseAction {
	
	UserAccountService service;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		LoginForm homeForm = (LoginForm) form;
		ActionForward forward = mapping.getInputForward();
		HttpSession session = request.getSession();
		
		if(session.getAttribute("userAccount") != null) {

			service = new UserAccountService();
			
			UserAccount userAccount = (UserAccount) session.getAttribute("userAccount");
			this.setVariables(userAccount, service.getEntityManager(), request);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("contacts.jsp");
			dispatcher.forward(request, response);
			
			return forward;
		}
		
		UserAccount userAccount = this.executeLogin(homeForm.getUsername(), homeForm.getPassword());
		
		if(userAccount != null) {
			
			session.setAttribute("userAccount", userAccount);
			session.setAttribute("userAccountId", userAccount.getUserAccountId());
			
			forward = mapping.findForward("success");
			
			this.setVariables(userAccount, service.getEntityManager(), request);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("contacts.jsp");
			dispatcher.forward(request, response);
			
		} else {
			ActionErrors err = new ActionErrors();
			err.add("", new ActionMessage("login.fail"));
			this.saveErrors(request, err);
		}

		return forward;
	}
	
	private UserAccount executeLogin(String username, String password) throws Exception {
		
		service = new UserAccountService();
		return service.validateUser(username, password);
	}
}
