package org.cabrera.ebookr.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.cabrera.ebookr.form.ContactsForm;
import org.cabrera.ebookr.model.UserAccount;
import org.cabrera.ebookr.services.ContactListService;

public class DeleteContactAction extends BaseAction {
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		ContactsForm contactForm = (ContactsForm) form;
		ActionForward forward = mapping.getInputForward();
		
		if(this.deleteContact(contactForm, request) == null) {
			
			forward = mapping.findForward("success");
		} else {
			
			ActionErrors err = new ActionErrors();
			err.add("", new ActionMessage("login.fail"));
			this.saveErrors(request, err);
		}

		return forward;
	}
	
	private String deleteContact(ContactsForm form, HttpServletRequest request) throws Exception {
		
		ContactListService service = new ContactListService();
		service.deleteContact(form.getContactListId());
		
		UserAccount userAccount = (UserAccount) request.getSession().getAttribute("userAccount");
		
		this.setVariables(userAccount, service.getEntityManager(), request);
		
		return null;
	}
}
