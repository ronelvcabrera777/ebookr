package org.cabrera.ebookr.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.cabrera.ebookr.form.RegisterForm;
import org.cabrera.ebookr.services.UserAccountService;

public class RegisterAction extends Action {
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		RegisterForm registerForm = (RegisterForm) form;
		ActionForward forward = mapping.getInputForward();
		
		if(this.createNewUser(registerForm) == null) {
			forward = mapping.findForward("success");
		} else {
			
			ActionErrors err = new ActionErrors();
			err.add("", new ActionMessage("login.fail"));
			this.saveErrors(request, err);
		}

		return forward;
	}
	
	public String createNewUser(RegisterForm register) {
		
		UserAccountService service = new UserAccountService();
		return service.createNewUser(register);
	}
}
