package org.cabrera.ebookr.action;

import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.Action;
import org.cabrera.ebookr.model.ContactList;
import org.cabrera.ebookr.model.UserAccount;
import org.cabrera.ebookr.services.AddressService;
import org.cabrera.ebookr.services.ContactListService;

public abstract class BaseAction extends Action {
	
	
	protected void setVariables(UserAccount userAccount, EntityManager entityManager, HttpServletRequest request) throws Exception {

		AddressService addressService = new AddressService(entityManager);
		List<ContactList> data = this.getUsersContacts(entityManager, userAccount, request);
		String filePath = this.getServlet().getServletContext().getRealPath("/") + "upload";
		
		request.setAttribute("filePath", filePath);
		request.setAttribute("countryList", addressService.getCountry());
		request.setAttribute("cityList", addressService.getAllCity());
		request.setAttribute("contactList", data);
		request.setAttribute("userAccount", userAccount);
	}
	

	protected List<ContactList> getUsersContacts(EntityManager entityManager, UserAccount userAccount, HttpServletRequest request) throws Exception {
		
		ContactListService service = new ContactListService(entityManager);
		service.setUserAccount(userAccount);
		List<ContactList> data = service.getAllContactListofUser();
		
		return data;
	}
}
