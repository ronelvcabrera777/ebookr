package org.cabrera.ebookr.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

public class ContactsListAction extends Action {
	
	@SuppressWarnings("unchecked")
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

        JSONObject returnObject = new JSONObject();
        String receivedMessage = "idc";
        String responseMessage = "Your Message has been received";
        int number = 100;
        returnObject.put("Sent Message", receivedMessage);
        returnObject.put("Response Message", responseMessage);
        returnObject.put("Number", number);
        

        java.io.PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        json.put("Sent Message", receivedMessage);
        json.put("Response Message", responseMessage);
        json.put("Number", number);
        out.write(json.toString());
        out.flush();
        
        return mapping.findForward( "success");
	}
}
