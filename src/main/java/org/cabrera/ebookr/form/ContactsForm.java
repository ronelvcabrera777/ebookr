package org.cabrera.ebookr.form;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class ContactsForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1267837919278272955L;
	
	private String firstName;
	private String lastName;
	private String middleName;
	private Integer city;
	private Integer country;
	private Integer createdById;
	private String mobileNumber;
	private FormFile profileImage;
	private FormFile signatureImage;
	private String newProfileImage;
	private String newSignatureImage;
	private Integer contactListId;
	private Integer updateContactListId;
	
	public ContactsForm() {
		
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Integer getCity() {
		return city;
	}

	public void setCity(Integer city) {
		this.city = city;
	}

	public Integer getCountry() {
		return country;
	}

	public void setCountry(Integer country) {
		this.country = country;
	}

	public Integer getCreatedById() {
		return createdById;
	}

	public void setCreatedById(Integer createdById) {
		this.createdById = createdById;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public FormFile getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(FormFile profileImage) {
		System.out.println(profileImage.getFileName());
		this.profileImage = profileImage;
	}

	public FormFile getSignatureImage() {
		return signatureImage;
	}

	public void setSignatureImage(FormFile signatureImage) {
		this.signatureImage = signatureImage;
	}

	public String getNewProfileImage() {
		return newProfileImage;
	}

	public void setNewProfileImage(String newProfileImage) {
		this.newProfileImage = newProfileImage;
	}

	public String getNewSignatureImage() {
		return newSignatureImage;
	}

	public void setNewSignatureImage(String newSignatureImage) {
		this.newSignatureImage = newSignatureImage;
	}
	
	public Integer getUpdateContactListId() {
		return updateContactListId;
	}

	public void setUpdateContactListId(Integer updateContactListId) {
		this.updateContactListId = updateContactListId;
	}

	public Integer getContactListId() {
		return contactListId;
	}

	public void setContactListId(Integer contactListId) {
		this.contactListId = contactListId;
	}
}
