package org.cabrera.ebookr.utils;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionFactory {

	public static Connection getConnection() {
		
		Context ctxLookup = null;
		DataSource ds = null;
		Connection conn= null;
		
		try {
			ctxLookup = new InitialContext();
			ds = (javax.sql.DataSource) ctxLookup.lookup("jdbc/ebookr");
		} catch (NamingException e) { 
			e.printStackTrace();
		}

		try {
			conn = ds.getConnection();
		} catch (SQLException e) { 
			e.printStackTrace();
		}
		
		return conn;
	}
}
