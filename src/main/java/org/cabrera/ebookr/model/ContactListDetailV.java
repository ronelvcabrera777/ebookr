package org.cabrera.ebookr.model;
// Generated 05 18, 18 4:23:38 AM by Hibernate Tools 5.2.3.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ContactListDetailV generated by hbm2java
 */
@Entity
@Table(name = "contact_list_detail_v", catalog = "ebookr")
public class ContactListDetailV implements java.io.Serializable {

	private int id;
	private String firstName;
	private String lastName;
	private String middleName;
	private String mobileNumber;
	private String countryName;
	private String cityName;
	private Integer zipCode;
	private String profileImage;
	private String signatureImage;

	public ContactListDetailV() {
	}

	public ContactListDetailV(int id) {
		this.id = id;
	}

	public ContactListDetailV(int id, String firstName, String lastName, String middleName, String mobileNumber,
			String countryName, String cityName, Integer zipCode, String profileImage, String signatureImage) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.mobileNumber = mobileNumber;
		this.countryName = countryName;
		this.cityName = cityName;
		this.zipCode = zipCode;
		this.profileImage = profileImage;
		this.signatureImage = signatureImage;
	}

	@Id

	@Column(name = "id", nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "first_name")
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "middle_name")
	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@Column(name = "mobile_number")
	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Column(name = "country_name")
	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Column(name = "city_name")
	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	@Column(name = "zip_code")
	public Integer getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}

	@Column(name = "profile_image")
	public String getProfileImage() {
		return this.profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	@Column(name = "signature_image")
	public String getSignatureImage() {
		return this.signatureImage;
	}

	public void setSignatureImage(String signatureImage) {
		this.signatureImage = signatureImage;
	}

}
