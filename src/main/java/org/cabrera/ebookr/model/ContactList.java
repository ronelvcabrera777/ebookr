package org.cabrera.ebookr.model;
// Generated 05 13, 18 4:12:54 PM by Hibernate Tools 5.2.3.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ContactList generated by hbm2java
 */
@Entity
@Table(name = "contact_list", catalog = "ebookr")
public class ContactList implements java.io.Serializable {

	private Integer contactListId;
	private AdCity adCity;
	private AdCountry adCountry;
	private UserAccount userAccount;
	private String firstName;
	private String lastName;
	private String middleName;
	private String mobileNumber;
	private Integer zipCode;
	private String slug;
	private String profileImage;
	private String signatureImage;
	private Date created;
	private Date updated;

	public ContactList() {
	}

	public ContactList(AdCity adCity, AdCountry adCountry, UserAccount userAccount, String firstName, String lastName,
			String middleName, String mobileNumber, Integer zipCode, String slug, String profileImage,
			String signatureImage, Date created, Date updated) {
		this.adCity = adCity;
		this.adCountry = adCountry;
		this.userAccount = userAccount;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.mobileNumber = mobileNumber;
		this.zipCode = zipCode;
		this.slug = slug;
		this.profileImage = profileImage;
		this.signatureImage = signatureImage;
		this.created = created;
		this.updated = updated;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "contact_list_id", unique = true, nullable = false)
	public Integer getContactListId() {
		return this.contactListId;
	}

	public void setContactListId(Integer contactListId) {
		this.contactListId = contactListId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ad_city_id")
	public AdCity getAdCity() {
		return this.adCity;
	}

	public void setAdCity(AdCity adCity) {
		this.adCity = adCity;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ad_country_id")
	public AdCountry getAdCountry() {
		return this.adCountry;
	}

	public void setAdCountry(AdCountry adCountry) {
		this.adCountry = adCountry;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by")
	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	@Column(name = "first_name")
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "middle_name")
	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@Column(name = "mobile_number")
	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Column(name = "zip_code")
	public Integer getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}

	@Column(name = "slug")
	public String getSlug() {
		return this.slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	@Column(name = "profile_image")
	public String getProfileImage() {
		return this.profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	@Column(name = "signature_image")
	public String getSignatureImage() {
		return this.signatureImage;
	}

	public void setSignatureImage(String signatureImage) {
		this.signatureImage = signatureImage;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created", length = 19)
	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated", length = 19)
	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}
