package org.cabrera.ebookr.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.cabrera.ebookr.utils.PersistenceUtil;

public abstract class BaseService {
	
	@PersistenceContext
	private EntityManager entityManager;

	public BaseService() {
		this.createEntityManager();
	}
	
	private void createEntityManager() {
		
		this.setEntityManager(PersistenceUtil.getEntityManagerFactory().createEntityManager());
	}
	
	public <T> T getModelById(Class<T> type, Integer id) {
		
		return this.getEntityManager().find(type, id);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getModelBy(Class<T> type, String classString, HashMap<String,String> filters) throws Exception {
		
		List<String> whereClauseList = new ArrayList<String>();

		for(Map.Entry<String, String> entry: filters.entrySet()) {
			
			String filterStr = entry.getKey() + " = :" + entry.getKey(); 
			whereClauseList.add(filterStr);
		}
		
		Query query = this.getEntityManager().createQuery("FROM " + classString + " WHERE " + String.join(" AND ", whereClauseList));
		
		for(Map.Entry<String, String> entry: filters.entrySet()) { 
			query.setParameter(entry.getKey(), entry.getValue());
		}
		
		try {
			
			return query.getResultList();
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}
