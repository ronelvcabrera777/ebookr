package org.cabrera.ebookr.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.cabrera.ebookr.model.AdCity;
import org.cabrera.ebookr.model.AdCountry;

public class AddressService extends BaseService {

	public AddressService() {

	}

	public AddressService(EntityManager entityManager) {
		this.setEntityManager(entityManager);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AdCity> getAllCity() throws Exception {
		
		Query query = this.getEntityManager().createQuery("FROM AdCity");
		
		try {
			List<AdCity> result = query.getResultList();
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<AdCountry> getCountry() throws Exception {
		
		Query query = this.getEntityManager().createQuery("FROM AdCountry");
		
		try {
			List<AdCountry> result = query.getResultList();
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
