package org.cabrera.ebookr.services;

import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import org.cabrera.ebookr.form.RegisterForm;
import org.cabrera.ebookr.model.UserAccount;

//@Stateless
public class UserAccountService extends BaseService {
	
	public UserAccountService() {
		
	}

	public UserAccountService(EntityManager entityManager) {
		this.setEntityManager(entityManager);
	}
	
	public String createNewUser(RegisterForm registerForm) {
		
		UserAccount user = new UserAccount();
		user.setFirstName(registerForm.getFirstName());
		user.setLastName(registerForm.getLastName());
		user.setEmailAddress(registerForm.getEmailAddress());
		user.setPassword(registerForm.getPassword());
		
		try {
			
			this.getEntityManager().getTransaction().begin();
			this.getEntityManager().persist(user);
			this.getEntityManager().getTransaction().commit();
			
		} catch(Exception e) {
			
			e.printStackTrace();
			this.getEntityManager().getTransaction().rollback();
			return e.getMessage();
		}
		
		return null;
	}
	
	@SuppressWarnings("serial")
	public UserAccount validateUser(String username, String password) throws Exception {
		
		HashMap<String, String> filters = new HashMap<String, String>();
		filters.put("emailAddress", username);
		filters.put("password", password);	
		
		List<UserAccount> user = this.getModelBy(UserAccount.class, "UserAccount", filters);
		
		if(user.isEmpty()) {
			return null;
		}
		
		return user.get(0);
	}
}