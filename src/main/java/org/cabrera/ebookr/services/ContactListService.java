package org.cabrera.ebookr.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.upload.FormFile;
import org.cabrera.ebookr.form.ContactsForm;
import org.cabrera.ebookr.model.AdCity;
import org.cabrera.ebookr.model.AdCountry;
import org.cabrera.ebookr.model.ContactList;
import org.cabrera.ebookr.model.ContactListDetailV;
import org.cabrera.ebookr.model.UserAccount;



public class ContactListService extends BaseService {

	private UserAccount userAccount;
	private AdCity adCity;
	private AdCountry adCountry;
	private Integer adContactListId;

	public ContactListService() {

	}

	public ContactListService(EntityManager entityManager) {
		this.setEntityManager(entityManager);
	}

	public String addNewContact(ContactsForm contact) {

		String profileImage = (contact.getProfileImage() == null) ? "defaultImage.jpg":contact.getNewProfileImage();
		String signatureImage = (contact.getSignatureImage() == null) ? "defaultImage.jpg":contact.getNewSignatureImage();
		String slug = contact.getFirstName().toLowerCase() + "-" + contact.getLastName().toLowerCase() + "-" + this.getUserAccount().getUserAccountId();

		ContactList newContact = new ContactList();
		newContact.setFirstName(contact.getFirstName());
		newContact.setLastName(contact.getLastName());
		newContact.setMiddleName(contact.getMiddleName());
		newContact.setMobileNumber(contact.getMobileNumber());
		newContact.setAdCountry(this.getAdCountry());
		newContact.setAdCity(this.getAdCity());
		newContact.setProfileImage(profileImage);
		newContact.setSignatureImage(signatureImage);
		newContact.setSlug(slug);
		newContact.setUserAccount(this.getUserAccount());

		try {

			this.getEntityManager().getTransaction().begin();
			this.getEntityManager().persist(newContact);
			this.getEntityManager().getTransaction().commit();

		} catch(Exception e) {

			e.printStackTrace();

			this.getEntityManager().getTransaction().rollback();
			this.getEntityManager().close();

			return e.getMessage();
		}

		return null;
	}
	
	public String updateContact(ContactsForm contact) {
		
		ContactList contactDetail = this.getEntityManager().find(ContactList.class, contact.getUpdateContactListId());

		String profileImage = (contact.getProfileImage() == null) ? contactDetail.getProfileImage():contact.getNewProfileImage();
		String signatureImage = (contact.getSignatureImage() == null) ? contactDetail.getSignatureImage():contact.getNewSignatureImage();
		String slug = contact.getFirstName().toLowerCase() + "-" + contact.getLastName().toLowerCase() + "-" + this.getUserAccount().getUserAccountId();
		
		contactDetail.setFirstName(contact.getFirstName());
		contactDetail.setLastName(contact.getLastName());
		contactDetail.setMiddleName(contact.getMiddleName());
		contactDetail.setMobileNumber(contact.getMobileNumber());
		contactDetail.setAdCountry(this.getAdCountry());
		contactDetail.setAdCity(this.getAdCity());
		contactDetail.setProfileImage(profileImage);
		contactDetail.setSignatureImage(signatureImage);
		contactDetail.setSlug(slug);
		contactDetail.setUserAccount(this.getUserAccount());
		
		
		try {

			this.getEntityManager().getTransaction().begin();
			this.getEntityManager().merge(contactDetail);
			this.getEntityManager().getTransaction().commit();

		} catch(Exception e) {

			e.printStackTrace();

			this.getEntityManager().getTransaction().rollback();
			this.getEntityManager().close();

			return e.getMessage();
		}

		return null;
	}
	
	public void deleteContact(Integer contactId) {
		
		ContactList contact = this.getEntityManager().find(ContactList.class, contactId);
		
		try {

			this.getEntityManager().getTransaction().begin();
			this.getEntityManager().remove(contact);
			this.getEntityManager().getTransaction().commit();
			
		} catch(Exception e) {

			e.printStackTrace();

			this.getEntityManager().getTransaction().rollback();
			this.getEntityManager().close();
		}
	}

	public String uploadFile(FormFile file, ActionServlet actionServlet) throws FileNotFoundException, IOException {
		
		if(file == null) {
			return null;
		}
		
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		
		String filePath = actionServlet.getServletContext().getRealPath("/") +"upload";
		
		File folder = new File(filePath);
		if(!folder.exists()){
			folder.mkdir();
		}

		File newFile = new File(filePath, timeStamp + "_" + file.getFileName());

		if(!newFile.exists()) {
			
			FileOutputStream fos = new FileOutputStream(newFile);
			fos.write(file.getFileData());
			fos.flush();
			fos.close();
		}
		
		return newFile.getName();
	}

	@SuppressWarnings("unchecked")
	public List<ContactList> getAllContactListofUser() throws Exception {

		String hql = "FROM ContactList WHERE userAccount = :userAccount";
		Query query = this.getEntityManager().createQuery(hql);
		query.setParameter("userAccount", this.getUserAccount());

		try {

			return query.getResultList();
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	@SuppressWarnings("unchecked")
	public List<ContactList> getAllContactListofUserModel() throws Exception {

		String hql = "FROM ContactList WHERE userAccount = :userAccount";
		Query query = this.getEntityManager().createQuery(hql);
		query.setParameter("userAccount", this.getUserAccount());

		try {
			List<ContactList> result = query.getResultList();
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public ContactListDetailV getContact(Integer contactListId) {
		
		ContactListDetailV contact = this.getEntityManager().find(ContactListDetailV.class, contactListId);
		
		return contact;
	}

	public UserAccount getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	public void setUserAccount(Integer userAccountId) {

		UserAccount userAccount = (UserAccount) this.getModelById(UserAccount.class, userAccountId);

		if(userAccount == null) {
			// Error - no account found
		}

		this.setUserAccount(userAccount);
	}

	public AdCountry getAdCountry() {
		return adCountry;
	}

	public void setAdCountry(AdCountry adCountry) {
		this.adCountry = adCountry;
	}


	public void setAdCountry(Integer adCountryId) {

		AdCountry adCountry = (AdCountry) this.getModelById(AdCountry.class, adCountryId);

		if(adCountry == null) {
			// Error - no account found
		}

		this.setAdCountry(adCountry);
	}

	public AdCity getAdCity() {
		return adCity;
	}

	public void setAdCity(AdCity adCity) {
		this.adCity = adCity;
	}

	public void setAdCity(Integer adCityId) {

		AdCity adCity = (AdCity) this.getModelById(AdCity.class, adCityId);

		if(adCity == null) {
			// Error - no account found
		}

		this.setAdCity(adCity);
	}

	public Integer getAdContactListId() {
		return adContactListId;
	}

	public void seTAdContactListId(Integer adContactListId) {
		this.adContactListId = adContactListId;
	}
}
